
#ifndef __KLT_RTL_SOLVE_LOOP_CONTEXT_H__
#define __KLT_RTL_SOLVE_LOOP_CONTEXT_H__

struct klt_loop_context_t * klt_build_loop_context(struct klt_loop_container_t * loop_desc, struct klt_loop_t * loops, struct kernel_t * kernel);

#endif /* __KLT_RTL_SOLVE_LOOP_CONTEXT_H__ */

